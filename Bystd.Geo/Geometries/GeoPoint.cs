﻿using System;

namespace Bystd.Geo.Geometries
{
    public class GeoPoint : Geometry
    {
        public double X { get { return ToCoordinate().X; } }

        public double Y { get { return ToCoordinate().Y; } }

        public double Z { get { return ToCoordinate().Z; } }

        public Coordinate ToCoordinate()
        {
            return GetSequence(0, 0)[0];
        }

        public GeoPoint(string wktString)
            : base(GeoType.POINT, wktString)
        {

        }

        public GeoPoint(double X, double Y, double Z = 0)
            : base(new Coordinate(X,Y,Z))
        {
 
        }

        public GeoPoint(Coordinate coord)
            : base(coord)
        {
 
        }

        public double Distance(Coordinate end)
        {
            return GetSequence(0, 0)[0].Distance(end);
        }

        public double Distance(GeoPoint point)
        {
            return ToCoordinate().Distance(point.ToCoordinate());
        }

        public double DistanceSquard(Coordinate end)
        {
            return GetSequence(0, 0)[0].DistanceSquard(end);
        }

        public double DistanceSquard(GeoPoint point)
        {
            return ToCoordinate().DistanceSquard(point.ToCoordinate());
        }

        public bool Equals(GeoPoint point)
        {
            return ToCoordinate().Equals(point.ToCoordinate());
        }

        public static bool operator <(GeoPoint s, GeoPoint d) => s < d;

        public static bool operator >(GeoPoint s, GeoPoint d) => s > d;

        public static bool operator <=(GeoPoint s, GeoPoint d) => s <= d;

        public static bool operator >=(GeoPoint s, GeoPoint d) =>s>=d;

        public GeoPoint Min(GeoPoint point, bool hasZ = false)
        {
            return new GeoPoint(this.ToCoordinate().Min(point.ToCoordinate(), hasZ));
        }

        public GeoPoint Max(GeoPoint point, bool hasZ = false)
        {
            return new GeoPoint(this.ToCoordinate().Max(point.ToCoordinate(), hasZ));
        }
    }
}
