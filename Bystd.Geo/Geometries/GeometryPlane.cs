﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bystd.Geo.Geometries
{
    using GeoParser;

    public class GeometryPlane:GeometryBase
    {
        public GeometryPlane(double lRadius = 6378137.0/*长半轴*/,
            double f = 0.00335281068118232/*递增变化量常量值(扁率)*/,
            int precision = 0/*精度保留小数点位*/)
            : base(lRadius, f, precision)
        {
            
        }

        public double Area(Coordinate[] coords)
        {
            bool isLB = coords[0].X < 360;

            if (isLB)
            {
                for (int i = 0; i < coords.Length; ++i)
                {
                    coords[i] = LBtoXY(coords[i]);//需要转换为平面直角坐标
                }
            }

            int len = coords.Length - 1;
            double s = 0, x, _y;

            double x0 = coords[0].X;

            for (int i = 1; i < len; ++i)
            {
                x = coords[i].X - x0;
                _y = (coords[i - 1].Y - coords[i + 1].Y);
                s += x * _y;
            }

            s *= 0.5;
            if (precision > 0)
            {
                s = Math.Round(s, precision);
            }
            return s;
        }

        public double Area(string wkt)
        {
            var wktParser = new WktParser(wkt);
            var colleciton = wktParser.FromReader();
 
            var sum =Math.Abs( Area(colleciton[0]));

            double holes = 0.0;
            //图形有洞
            for (int i = 1; i < colleciton.Count; ++i)
            {
                holes +=Math.Abs( Area(colleciton[i]));
            }

            return sum - holes;
        }
    }
}
