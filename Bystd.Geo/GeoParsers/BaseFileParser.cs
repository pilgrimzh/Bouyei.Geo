﻿using System;
using System.Text;

namespace Bystd.Geo.GeoParser
{
    public class BaseFileParser : IParser
    {
        protected string filename = string.Empty;
        protected Encoding encoding = Encoding.UTF8;

        public BaseFileParser(string filename)
        {
            this.filename = filename;
        }
        public BaseFileParser(string filename,Encoding encoding)
            :this(filename)
        {
            this.encoding = encoding;
        }

        public virtual void Dispose()
        { }
    }
}
