﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bystd.Geo.GeoParser
{
     public class BaseBytesParser:IParser
    {
        protected byte[] array = null;
        public BaseBytesParser(byte[] array)
        {
            this.array = array;
        }

        public virtual void Dispose()
        {
            if (array != null)
                array = null;
        }
    }
}
