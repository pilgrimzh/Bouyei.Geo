﻿using System;
using System.Text;
using System.Text.Json;

namespace Bystd.Geo.Converters
{
    public class JsonExtensions
    {
        //private Encoding encoding = Encoding.UTF8;
        public JsonExtensions(Encoding encoding=null)
        {
            //if(encoding!=null)
            //{
            //    this.encoding = encoding;
            //}
        }

        public string ConvertFrom<T>(T objectValue)
        {
            return JsonSerializer.Serialize<T>(objectValue);
        }

        public T ConvertTo<T>(string jsonString)
        {
            return JsonSerializer.Deserialize<T>(jsonString);
        }
    }
}
