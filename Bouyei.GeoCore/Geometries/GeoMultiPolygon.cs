﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouyei.GeoCore.Geometries
{
    public class GeoMultiPolygon:Geometry
    {
        public GeoMultiPolygon(string wktString)
            :base(GeoType.MULTIPOLYGON,wktString)
        { }

        public GeoMultiPolygon(GeoPolygon[] polygons)
            : base(polygons)
        {

        }

        public GeoPolygon this[int index]
        {
            get {
                var seqs = GetGemoetry(index);
                return new GeoPolygon(seqs.ToArray());
            }
        }
    }
}
