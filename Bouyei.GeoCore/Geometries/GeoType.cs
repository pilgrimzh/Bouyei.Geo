﻿using System;

namespace Bouyei.GeoCore.Geometries
{
    [Flags]
    public enum GeoType
    {
        Unknown=0,
        POINT=1,
        LINESTRING=2,
        POLYGON=3,
        MULTIPOINT=4,
        MULTILINESTRING=5,
        MULTIPOLYGON=6,
        GEOMETRYCOLLECTION=7,
        LineSegment=65535
    }
}
