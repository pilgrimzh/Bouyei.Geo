﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Reflection;

namespace Bouyei.GeoCore.GeoParser
{
    public class VctParser : BaseFileParser
    {
        public VctParser(string vctFile)
             : base(vctFile)
        {

        }

        public void FromReader()
        {
            using(FileStream fs=new  FileStream(filename,FileMode.Open))
            using(StreamReader reader=new StreamReader(fs,encoding))
            {
                var header = new VctHeadItem();
                bool isHeader = header.FromReader(reader);
                
            }
        }
    }

    #region structure
    public class VctInfo
    {
        public VctHeadItem vctHeader { get; set; }

        public VctFeatureCodeItem vctFeatureCode { get; set; }

       public VctTableStructureItem vctTableStructure { get; set; }

        public VctGeoGraphicItem vctGeoGraphic { get; set; }

        public VctAnnontationItem vctAnnontation { get; set; }

        public VctTopologyItem vctTopology { get; set; }

        public VctAttributeItem vctAttribute { get; set; }

        public VctStyleItem vctStyle { get; set; }
    }

    public class VctHeadItem
    {
        public string DataMark { get; set; }

        public string Version { get; set; }

        public string CoordinateSystemType { get; set; }
        public int Dim { get; set; }
        public string XAxisDirection { get; set; }

        public string YAxisDirection { get; set; }

        public string XYUnit { get; set; }
        public string ZUnit { get; set; }
        /// <summary>
        /// format CGCS2000,6378137.0,298.257222101
        /// </summary>
        public string Spheroid { get; set; }

        public string PrimeMeridian { get; set; }
        /// <summary>
        /// 高斯-克吕格投影
        /// </summary>
        public string Projection { get; set; }
        /// <summary>
        /// 108.000000,1,36500000.000000,0.000000,3,36
        /// </summary>
        public string Parameters { get; set; }
        /// <summary>
        /// 国家高程基准
        /// </summary>
        public string VerticalDatum { get; set; }

        public string TemporalReferenceSystem { get; set; }
        /// <summary>
        /// 36487405.230300,2963266.548800
        /// </summary>
        public string ExtentMin { get; set; }
        public string ExtentMax { get; set; }

        public int MapScale { get; set; }
        /// <summary>
        /// 0,0
        /// </summary>
        public string Offset { get; set; }
        /// <summary>
        /// 20200119
        /// </summary>
        public string Date { get; set; }
        /// <summary>
        /// ,
        /// </summary>
        public string Separator { get; set; }

        public bool FromReader(StreamReader reader)
        {
            var thisType = this.GetType();
            var types =thisType.GetProperties(BindingFlags.Public | BindingFlags.GetField | BindingFlags.SetField);

            bool isTrue = false;
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                if (line == string.Empty) continue;
                else if (line == "HeadBegin") continue;
                else if (line == "HeadEnd") break;

                var array = line.Trim().Split(':');
                if (array.Length == 1) continue;

                thisType.GetField(array[0])?.SetValue(array[1], null);
                isTrue = true;
            }
            return isTrue;
        }
    }

    public class VctFeatureCodeItem
    {
        public VctFCodeItem[] FItem { get; set; }
    }

    public class VctFCodeItem
    {
        public string FCode { get; set; }

        public string FName { get; set; }

        public string FType { get; set; }

        public int FColor { get; set; }

        public string FTableName { get; set; }
    }

    public class VctTableStructureItem
    {
        public string TableName { get; set; }

        public int ColumnCount { get; set; }

        public List<VctTTableItem> Columns { get; set; }
    }

    public class VctTTableItem
    {
        public string ColumnName { get; set; }

        public string ColumnType { get; set; }

        public int ColumnSize { get; set; }
    }

    public class VctGeoGraphicItem
    {
        public VctPointItem Point { get; set; }

        public VctLineItem Line { get; set; }

        public VctPointItem Polygon { get; set; }
    }

    public class GPoint
    {
        public double X;
        public double Y;
    }

    public class VctPointItem : VctGraphBase
    {
        /// <summary>
        /// 要素类型,1为单点
        /// </summary>
        public int ValueType { get; set; }
        /// <summary>
        /// 坐标点集合
        /// </summary>
        public List<GPoint> Points { get; set; }
    }

    public class VctLineItem : VctGraphBase
    {
        public string FNo { get; set; }

        public int Segs { get; set; }

        public List<VctSegItem> SegItems { get; set; }
    }

    public class VctSegItem
    {
        public int SegType { get; set; }

        public int SegPoints { get; set; }

        public List<GPoint> Points { get; set; }
    }

    public class VctPolygonItem : VctLineItem
    {

    }

    public class VctGraphBase
    {
        /// <summary>
        /// 标识码
        /// </summary>
        public int ICode { get; set; }
        /// <summary>
        /// 要素类
        /// </summary>
        public string FType { get; set; }
        /// <summary>
        /// 图形表现码
        /// </summary>
        public string GRCode { get; set; }

        /// <summary>
        /// 颜色值
        /// </summary>
        public int IColor { get; set; }
    }

    public class VctAnnontationItem
    {
        /// <summary>
        /// 颜色值
        /// </summary>
        public int IColor { get; set; }
        /// <summary>
        /// 注记要素项
        /// </summary>
        public VctAnnontationBase Items { get; set; }
    }

    public class VctAnnontationBase
    {
        /// <summary>
        /// 标识码
        /// </summary>
        public int ICode { get; set; }
        /// <summary>
        /// 要素类
        /// </summary>
        public string FType { get; set; }
        /// <summary>
        /// 图形表现码
        /// </summary>
        public string GRCode { get; set; }
        /// <summary>
        /// 要素编号
        /// </summary>
        public string FNo { get; set; }
        /// <summary>
        /// 标识内容
        /// </summary>
        public string RContent { get; set; }
        /// <summary>
        /// 注记数
        /// </summary>
        public int PointCount { get; set; }
        /// <summary>
        /// 点数
        /// </summary>
        public List<VctAnnontaionPoint> Points { get; set; }
    }

    public class VctAnnontaionPoint
    {
        public double X { get; set; }
        public double Y { get; set; }

        public double Angle { get; set; }
    }

    public class VctTopologyItem
    {
        public VctNodeTopology TopoNodes { get; set; }

        public VctEdgeTopology TopoEdage { get; set; }
    }

    public class VctNodeTopology
    {
        public List<VctNodeTopologyBase> NodePologys { get; set; }
    }

    public class VctNodeTopologyBase
    {
        public int ICode { get; set; }

        public int PointCount { get; set; }

        public int[] PointNo { get; set; }

        public int PColor { get; set; }
    }

    public class VctEdgeTopology
    {
        public List<VctEdgeTopologyBase> Egdes { get; set; }
    }

    public class VctEdgeTopologyBase
    {
        public int ICode { get; set; }

        public int StartNo { get; set; }

        public int EndNo { get; set; }

        public int LeftNo { get; set; }

        public int RightNo { get; set; }
    }

    public class VctAttributeItem
    {
        public List<string> TableItems { get; set; }

        public List<string> VarcharItems { get; set; }
    }

    public class VctStyleItem
    {
        public List<VctRepresentationItem> RepresentationItems { get; set; }
    }

    public class VctRepresentationItem
    {
        public string Style { get; set; }

        public string Name { get; set; }

        public int Number { get; set; }

        public int Color { get; set; }

        public int Symbolid { get; set; }

        public float PointSize { get; set; }

        public int FColor { get; set; }
    }
    #endregion
}
