﻿using Bouyei.GeoCore.Geometries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouyei.GeoCore.GeoParser
{
    public class BaseStringParser:IParser
    {
        public string content = string.Empty;
        public BaseStringParser(string content)
        {
            this.content = content;
        }

        public virtual void Dispose()
        { }
    }
}
