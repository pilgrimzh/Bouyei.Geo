﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouyei.Geo.Geometries
{
    public interface IGeometry
    {
        GeoType GeometryType { get; set; }

        int GeometryCount { get; }

        GeoSequence GetSequence(int geoIndex,int index);

        List<GeoSequence> GetGemoetry(int geoIndex);
    }
}
