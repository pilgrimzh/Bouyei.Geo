﻿using System;

namespace Bouyei.Geo.Geometries
{
    public class GeoRectangle
    {
        public GeoLineSegment Top { get; set; }

        public GeoLineSegment Bottom { get; set; }

        public GeoRectangle()
        { }

        public GeoRectangle(GeoPoint TL, GeoPoint TR, GeoPoint BL, GeoPoint BR)
            :this()
        {
            Top = new GeoLineSegment(TL, TR);
            Bottom = new GeoLineSegment(BL, BR);
        }

        public GeoRectangle(Coordinate TL, Coordinate TR, Coordinate BL, Coordinate BR)
            :this()
        {
            Top = new GeoLineSegment(TL, TR);
            Bottom = new GeoLineSegment(BL, BR);
        }

        public GeoLineSegment GetMinMax()
        {
            var min = Top.Start.Min(Top.End);
            var max = Top.Start.Max(Top.End);

            min = min.Min(Bottom.Start);
            max = max.Max(Bottom.Start);

            min = min.Min(Bottom.End);
            max = max.Max(Bottom.End);

            return  new GeoLineSegment(min, max);
        }

        public GeoLineSegment GetDiagonal()
        {
            return new GeoLineSegment(Top.Start, Bottom.End);
        }

        public bool Intersects(GeoRectangle other)
        {
            var mmother = other.GetMinMax();
            var mmthis = GetMinMax();

            return (mmother.Start < mmthis.End)
                && (mmother.End > mmthis.Start);
        }

        public bool Intersects(GeoPoint point)
        {
            var minmax = GetMinMax();

            return (point <= minmax.End && point >= minmax.Start);
        }
    }
}
