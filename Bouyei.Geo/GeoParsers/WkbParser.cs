﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouyei.Geo.GeoParsers
{
    using Geometries;
    using Converters;
    using System.Collections;

    public class WkbParser : BaseBytesParser
    {
        private bool isLittleEndian = false;
        private BitExtensions bitConvert = null;

        public GeoType geometryType { get; set; }

        public WkbParser(byte[] array)
            : base(array)
        {
            this.array = array;

            isLittleEndian = array[0] == 1 ? true : false;
            bitConvert = new BitExtensions(isLittleEndian);
        }

        public WkbParser()
            :base(null)
        {

        }

        public unsafe Geometry FromReader()
        {
            fixed (byte* ptr = &array[1])
            {
                geometryType = (GeoType)bitConvert.ToInt32(ptr);
                byte* start = ptr + 4;

                switch (geometryType)
                {
                    case GeoType.POINT:
                        {
                            var point = new WkbPoint(bitConvert, start);
                            return new Geometry(point.Coord);
                        }
                    case GeoType.MULTIPOINT:
                        {
                            var multipoint = new WkbMultiPoint(bitConvert, start);
                            return new Geometry(GeoType.MULTIPOINT, multipoint.GetCoordinates());
                        }
                    case GeoType.LINESTRING:
                        {
                            var linestring = new WkbLineString(bitConvert, start);
                            return new Geometry(GeoType.LINESTRING, linestring.GetCoordinates());
                        }
                    case GeoType.MULTILINESTRING:
                        {
                            var multilinestring = new WkbMultiLineString(bitConvert, start);
                            return new Geometry(GeoType.MULTILINESTRING, multilinestring.GetCoordinates());
                        }
                    case GeoType.POLYGON:
                        {
                            var polygon = new WkbPolygon(bitConvert, start);
                            if (polygon.Count == 1) return new Geometry(GeoType.POLYGON, polygon[0].Coords);
                            else if (polygon.Count > 1) return new Geometry(polygon[0].Coords, polygon.GetInteriorRing());
                            break;
                        }
                    case GeoType.MULTIPOLYGON:
                        {
                            var multiPolygon = new WkbMultiPolygon(bitConvert, start);
                            List<GeoPolygon> polygons = new List<GeoPolygon>(multiPolygon.Count);

                            foreach (WkbPolygon polygon in multiPolygon)
                            {
                                if (polygon.Count == 1)
                                    polygons.Add(new GeoPolygon(polygon.GetOuterRing()));
                                else if (polygon.Count >= 2)
                                    polygons.Add(new GeoPolygon(polygon.GetOuterRing(), polygon.GetInteriorRing()));
                            }
                            return new Geometry(polygons.ToArray());
                        }
                    default:
                        throw new Exception("geometryType not supported" + geometryType);
                }
            }
            return null;
        }

        public unsafe byte[] ToWriter(Geometry geometry)
        {
           switch(geometry.GeometryType)
            {
                case GeoType.POINT:
                    {
                        byte[] buffer = new byte[1 + 4 + 16];
                        array[0] = 1;

                        fixed (byte* ptr = &array[1])
                        {
                            *(int*)(ptr) = (int)geometry.GeometryType;

                            WkbPoint.ToWriter(geometry, ptr + 4);
                        }
                        return buffer;
                    }
                case GeoType.MULTIPOINT:
                    {
                        int pCount = geometry.GetSequence(0, 0).Count;
                        byte[] buffer = new byte[1 + 4 + 4 + 16 * pCount];
                        buffer[0] = 1;

                        fixed (byte* ptr = &buffer[1])
                        {
                            *(int*)(ptr) = (int)geometry.GeometryType;

                            WkbMultiPoint.ToWriter(geometry, ptr + 4);
                        }

                        return buffer;
                    }
                case GeoType.LINESTRING:
                    {
                        int pCount = geometry.GetSequence(0, 0).Count;
                        byte[] buffer = new byte[1 + 4 + 4 + 16 * pCount];
                        buffer[0] = 1;

                        fixed (byte* ptr = &buffer[1])
                        {
                            *(int*)(ptr) = (int)geometry.GeometryType;

                            WkbLineString.ToWriter(geometry, ptr + 4);
                        }

                        return buffer;
                    }
                case GeoType.MULTILINESTRING:
                    {

                        var seqs = geometry.GetGemoetry(0);
                        int pCount = seqs.Count * 4;

                        for (int i = 0; i < seqs.Count; ++i)
                            for (int j = 0; j < seqs[i].Count; ++j)
                                pCount += 20;

                        byte[] buffer = new byte[1 + 4 + pCount];
                        buffer[0] = 1;

                        fixed (byte* ptr = &buffer[1])
                        {
                            *(int*)(ptr) = (int)geometry.GeometryType;

                            WkbMultiLineString.ToWriter(geometry, ptr + 4);
                        }

                        return buffer;
                    }
                case GeoType.POLYGON:
                    {
                        var seqs = geometry.GetGemoetry(0);
                        int pCount = seqs.Count * 4;

                        for (int i = 0; i < seqs.Count; ++i)
                            for (int j = 0; j < seqs[i].Count; ++j)
                                pCount += 20;

                        byte[] buffer = new byte[1 + 4 + pCount];
                        buffer[0] = 1;

                        fixed (byte* ptr = &buffer[1])
                        {
                            *(int*)(ptr) = (int)geometry.GeometryType;

                            WkbPolygon.ToWriter(geometry, ptr + 4);
                        }
                        return buffer;
                    }
                case GeoType.MULTIPOLYGON:
                    {
                        int pCount = geometry.GeometryCount * 4;

                        for (int j = 0; j < geometry.GeometryCount; ++j)
                        {
                            var seqs = geometry.GetGemoetry(j);
                            pCount += seqs.Count * 4;

                            for (int k = 0; k < seqs.Count; ++k)
                                for (int i = 0; i < seqs[k].Count; ++i)
                                    pCount += 20;
                        }

                        byte[] buffer = new byte[1 + 4 + pCount];
                        buffer[0] = 1;

                        fixed (byte* ptr = &buffer[1])
                        {
                            *(int*)(ptr) = (int)geometry.GeometryType;

                            WkbMultiPolygon.ToWriter(geometry, ptr + 4);
                        }
                        return buffer;
                    }
                default:throw new Exception("not supported:" + geometry.GeometryType.ToString());
            }
        }
    }

    public class WkbBase
    {
        protected BitExtensions bitConvert = null;

        public WkbBase() { }

        public unsafe WkbBase(BitExtensions bitConvert)
        {
            this.bitConvert = bitConvert;
        }
    }

    public class WkbPoint:WkbBase
    {
        public Coordinate Coord { get; set; }
        public WkbPoint() { }

        public WkbPoint(byte[] array)
        {
            bool isLittleEndian = array[0] == 1 ? true : false;
            bitConvert = new BitExtensions(isLittleEndian);

            unsafe
            {
                fixed (byte* ptr = &array[1])
                {
                    var geometryType = (GeoType)bitConvert.ToInt32(ptr);
                    if (geometryType != GeoType.POINT) throw new Exception("geometry type is " + geometryType.ToString());

                    byte* start = ptr + 4;

                    Coord = new Coordinate()
                    {
                        X = bitConvert.ToDouble(ptr),
                        Y = bitConvert.ToDouble(ptr + 8)
                    };
                }
            }
        }

        public unsafe WkbPoint(BitExtensions bitConvert,byte* ptr)
            :base(bitConvert)
        {
            Coord = new Coordinate()
            {
                X = bitConvert.ToDouble(ptr),
                Y = bitConvert.ToDouble(ptr + 8)
            };
        }

        public static unsafe void ToWriter(Geometry geometry, byte* ptr)
        {
            var seq = geometry.GetSequence(0, 0);

            double t = seq[0].X;
            *(long*)ptr = *(long*)(&t);

            t = seq[0].Y;
            *(long*)(ptr + 8) = *(long*)(&t);
        }
    }

    public class WkbMultiPoint:WkbBase,IEnumerable
    {
        public WkbPoint[] Points { get; set; }

        public Int32 Count { get; set; }

        public WkbPoint this[int index]
        {
            get {return Points[index]; }
            set { Points[index] = value; }
        }


        public WkbMultiPoint()
        { }

        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }

        public WkbMultiPoint(byte[] array)
        {
            bool isLittleEndian = array[0] == 1 ? true : false;
            bitConvert = new BitExtensions(isLittleEndian);

            unsafe
            {
                fixed (byte* ptr = &array[1])
                {
                    var geometryType = (GeoType)bitConvert.ToInt32(ptr);
                    if (geometryType != GeoType.MULTIPOINT) throw new Exception("geometry type is " + geometryType.ToString());

                    Count = bitConvert.ToInt32(ptr + 4);
                    Points = new  WkbPoint[Count];
                    
                    int start = 8;
                    for (int i = 0; i < Count; ++i)
                    {
                        Points[i] = new WkbPoint(bitConvert, ptr + start);
                        start += 16;
                    }
                }
            }
        }

        public unsafe WkbMultiPoint(BitExtensions bitConvert,byte *ptr)
            :base(bitConvert)
        {
            Count = bitConvert.ToInt32(ptr);
            Points = new WkbPoint[Count];

            int start = 4;
            for (int i = 0; i < Count; ++i)
            {
                WkbPoint point = new WkbPoint(bitConvert,ptr + start);
                start += 16;
            }
        }

        public static unsafe void ToWriter(Geometry geometry, byte* ptr)
        {
            var seq = geometry.GetSequence(0, 0);
            int c = seq.Count;
            *(int*)ptr = c;

            ptr += 4;

            for (int i = 0,step=0; i < c; ++i)
            {
                double t = seq[i].X;
                *(long*)(ptr + step) = *(long*)(&t);

                t = seq[i].Y;
                *(long*)(ptr +step+ 8) = *(long*)(&t);

                step += 16;
            }           
        }

        public Coordinate[] GetCoordinates()
        {
            return Points.Select(x => x.Coord).ToArray();
        }
    }

    public class WkbLineString:WkbBase
    {
        public Int32 Count { get; set; }

        private Coordinate[] Coords { get; set; }

        public Coordinate this[int index]
        {
            get {return Coords[index]; }
            set { Coords[index] = value; }
        }
 
        public WkbLineString()
        { }

        private unsafe void ParserFromArray(byte *ptr)
        {
            Count = bitConvert.ToInt32(ptr);
            Coords = new Coordinate[Count];

            int start = 4;
            for (int i = 0; i < Count; ++i)
            {
                Coords[i] = new Coordinate()
                {
                    X = bitConvert.ToDouble(ptr + start),
                    Y = bitConvert.ToDouble(ptr + start + 8)
                };
                start += 16;
            }
        }

        public WkbLineString(byte[] array)
        {
            bool isLittleEndian = array[0] == 1 ? true : false;
            bitConvert = new BitExtensions(isLittleEndian);

            unsafe
            {
                fixed (byte* ptr = &array[1])
                {
                    Count = bitConvert.ToInt32(ptr);

                    var geometryType = (GeoType)bitConvert.ToInt32(ptr+4);
                    if (geometryType != GeoType.LINESTRING) throw new Exception("geometry type is " + geometryType.ToString());

                    byte* start = ptr + 8;
                    ParserFromArray(start);
                }
            }
        }

        public unsafe WkbLineString(BitExtensions bitConvert, byte *ptr)
            :base(bitConvert)
        {
            ParserFromArray(ptr);
        }

        public static unsafe void ToWriter(Geometry geometry, byte* ptr)
        {
            var seq = geometry.GetSequence(0, 0);
            int c = seq.Count;
            *(int*)ptr = c;

            ptr += 4;

            for (int i = 0, step = 0; i < c; ++i)
            {
                double t = seq[i].X;
                *(long*)(ptr + step) = *(long*)(&t);

                t = seq[i].Y;
                *(long*)(ptr + step + 8) = *(long*)(&t);

                step += 16;
            }
        }

        public Coordinate[] GetCoordinates()
        {
            return Coords;
        }
    }

    public class WkbMultiLineString:WkbBase,IEnumerable
    {
        public Int32 Count { get; set; }

        private List<WkbLineString> MultiLineString { get; set; }

        public WkbLineString this[int index]
        {
            get { return MultiLineString[index]; }
            set { MultiLineString[index] = value; }
        }

        public WkbMultiLineString()
        { }

        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }

        public WkbMultiLineString(byte[] array)
        {
            bool isLittleEndian = array[0] == 1 ? true : false;
            bitConvert = new BitExtensions(isLittleEndian);

            unsafe
            {
                fixed (byte* ptr = &array[1])
                {
                    Count = bitConvert.ToInt32(ptr);

                    var geometryType = (GeoType)bitConvert.ToInt32(ptr + 4);
                    if (geometryType != GeoType.MULTILINESTRING) throw new Exception("geometry type is " + geometryType.ToString());

                    byte* start = ptr + 8;
                    ParserFromArray(start);
                }
            }
        }

        public unsafe WkbMultiLineString(BitExtensions bitConvert,byte *ptr)
            :base(bitConvert)
        {
            ParserFromArray(ptr);
        }

        public static unsafe void ToWriter(Geometry geometry, byte* ptr)
        {
            var geos = geometry.GetGemoetry(0);
            
            int count = geos.Count;
            *(int*)ptr = count;
            ptr += 4;

            for (int i = 0; i < count; ++i)
            {
                var seq = geos[i];
                int sCount = seq.Count;

                *(int*)ptr = sCount;
                ptr += 4;
                int j = 0, step = 0;

                for (; j < sCount; ++j)
                {
                    double t = seq[j].X;
                    *(long*)(ptr + step) = *(long*)(&t);

                    t = seq[j].Y;
                    *(long*)(ptr + step + 8) = *(long*)(&t);

                    step += 16;
                }
                ptr += step;
            }
        }


        public List<Coordinate[]> GetCoordinates()
        {
            List<Coordinate[]> coords = new List<Coordinate[]>(Count);
            foreach (var item in MultiLineString)
            {
                coords.Add(item.GetCoordinates());
            }
            return coords;
        }

        private unsafe void ParserFromArray(byte* ptr)
        {
            Count = bitConvert.ToInt32(ptr);
            MultiLineString = new List<WkbLineString>(Count);

            int start = 4;
            for (int j = 0; j < Count; ++j)
            {
                WkbLineString linestring = new WkbLineString(bitConvert, ptr + start);
                start += (linestring.Count * 16);
                MultiLineString.Add(linestring);
            }
        }
    }

    public class WkbPolygon : WkbBase
    {
        public Int32 Count { get; set; }

        private LinearRing[] Rings { get; set; }

        public LinearRing this[int index]
        {
            get { return Rings[index]; }
            set { Rings[index] = value; }
        }

        public WkbPolygon()
        { }

        public WkbPolygon(byte[] array)
        {
            bool isLittleEndian = array[0] == 1 ? true : false;
            bitConvert = new BitExtensions(isLittleEndian);

            unsafe
            {
                fixed (byte* ptr = &array[1])
                {
                    Count = bitConvert.ToInt32(ptr);

                    var geometryType = (GeoType)bitConvert.ToInt32(ptr + 4);
                    if (geometryType != GeoType.POLYGON) throw new Exception("geometry type is " + geometryType.ToString());

                    byte* start = ptr + 8;
                    ParserFromArray(start);
                }
            }
        }

        private unsafe void ParserFromArray(byte *ptr)
        {
            //ring count
            Count = bitConvert.ToInt32(ptr);
            Rings = new LinearRing[Count];
            int _start = 4;

            for (int i = 0; i < Count; ++i)
            {
                int start = _start;
                int pCount = bitConvert.ToInt32(ptr + start);

                LinearRing linearing = new LinearRing(pCount);
                start += 4;

                for (int j = 0; j < pCount; ++j)
                {
                    linearing.Coords[j] = new Coordinate()
                    {
                        X = bitConvert.ToDouble(ptr + start),
                        Y = bitConvert.ToDouble(ptr + start + 8)
                    };

                    start += 16;
                }
                Rings[i] = linearing;

                _start += (4 + (pCount * 16));
            }
        }

        public unsafe WkbPolygon(BitExtensions bitConvert, byte* ptr)
            : base(bitConvert)
        {
            ParserFromArray(ptr);
        }

        public static unsafe void ToWriter(Geometry geometry, byte* ptr)
        {
            var geos = geometry.GetGemoetry(0);

            int count = geos.Count;
            *(int*)ptr = count;
            ptr += 4;

            for (int i = 0; i < count; ++i)
            {
                var seq = geos[i];
                int sCount = seq.Count;

                *(int*)ptr = sCount;
                ptr += 4;
                int j = 0, step = 0;

                for (; j < sCount; ++j)
                {
                    double t = seq[j].X;
                    *(long*)(ptr + step) = *(long*)(&t);

                    t = seq[j].Y;
                    *(long*)(ptr + step + 8) = *(long*)(&t);

                    step += 16;
                }
                ptr += step;
            }
        }
 
        public Coordinate[] GetOuterRing()
        {
            if (Count >=1) return Rings[0].Coords;
            return null;
        }

        public List<Coordinate[]> GetInteriorRing()
        {
            List<Coordinate[]> interior = new List<Coordinate[]>(Count - 1);
            if (Count >= 2)
            {
                for(int i=1;i<Rings.Length;++i)
                {
                    interior.Add(Rings[i].Coords);
                }
            }
            return interior;
        }
    }

    public class WkbMultiPolygon:WkbBase,IEnumerable
    {
        public Int32 Count { get; set; }

        private List<WkbPolygon> MultiPolygon { get; set; }

        public WkbPolygon this[int index]
        {
            get { return MultiPolygon[index]; }
            set { MultiPolygon[index] = value; }
        }

        public WkbMultiPolygon()
        { }

        public IEnumerator GetEnumerator()
        {
            return(IEnumerator)this;
        }

        public WkbMultiPolygon(byte[] array)
        {
            bool isLittleEndian = array[0] == 1 ? true : false;
            bitConvert = new BitExtensions(isLittleEndian);

            unsafe
            {
                fixed (byte* ptr = &array[1])
                {
                    Count = bitConvert.ToInt32(ptr);

                    var geometryType = (GeoType)bitConvert.ToInt32(ptr + 4);
                    if (geometryType != GeoType.MULTIPOLYGON) throw new Exception("geometry type is " + geometryType.ToString());

                    byte* start = ptr + 8;
                    ParserFromArray(start);
                }
            }
        }

        public unsafe WkbMultiPolygon(BitExtensions bitConvert,byte *ptr)
            :base(bitConvert)
        {
            ParserFromArray( ptr);
        }

        public static unsafe void ToWriter(Geometry geometry, byte* ptr)
        {
            var gCount = geometry.GeometryCount;
            for (int k = 0; k < gCount; ++k)
            {
                var geos = geometry.GetGemoetry(k);

                int count = geos.Count;
                *(int*)ptr = count;
                ptr += 4;

                for (int i = 0; i < count; ++i)
                {
                    var seq = geos[i];
                    int sCount = seq.Count;

                    *(int*)ptr = sCount;
                    ptr += 4;
                    int j = 0, step = 0;

                    for (; j < sCount; ++j)
                    {
                        double t = seq[j].X;
                        *(long*)(ptr + step) = *(long*)(&t);

                        t = seq[j].Y;
                        *(long*)(ptr + step + 8) = *(long*)(&t);

                        step += 16;
                    }
                    ptr += step;
                }
            }
        }

        private unsafe void ParserFromArray(byte* ptr)
        {
            Count = bitConvert.ToInt32(ptr);
            MultiPolygon = new List<WkbPolygon>(Count);

            int start = 4;
            for (int j = 0; j < Count; ++j)
            {
                WkbPolygon polygon = new  WkbPolygon(bitConvert, ptr + start);
                start += (polygon.Count * 16);
                MultiPolygon.Add(polygon);
            }
        }
    }

    public class LinearRing
    {
        public Int32 Count { get; set; }

        public Coordinate[] Coords { get; set; }

        public LinearRing() { }

        public LinearRing(Int32 Count)
        {
            this.Count = Count;
            Coords = new Coordinate[Count];
        }
    }
}
