﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouyei.Geo.GeoParsers
{
    public class BaseBytesParser:IParser
    {
        protected byte[] array = null;
        public BaseBytesParser(byte[] array)
        {
            this.array = array;
        }

        public virtual void Dispose()
        {
            if (array != null)
                array = null;
        }
    }
}
