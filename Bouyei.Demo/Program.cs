﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Bouyei.Geo;
using System.IO;
using Bouyei.DbFactory;
 
namespace Bouyei.Demo
{
    using Bouyei.Geo.Geometries;
    using Bouyei.Geo.GeoParsers;

    class Program
    {
        static void Main(string[] args)
        {
            TestVctFormat();

            //TestGeoJson();

           //var item=JsonPaser.Json.Parse("{\"type\": \"Feature\",\"geometry\": {\"type\": \"Point\",\"coordinates\": [125.6, 10.1]},\"properties\": {\"name\": \"Dinagat Islands\"}}");

            //TestFromEsriMdb();

            TestWkbFromPg();

            TestArea();
        }

        static void TestWkbFromPg()
        {
            string connstr = "Server=127.0.0.1;Port=5432;User Id=postgres;Password=bouyei;Database=geodb;";
            using (IAdoProvider provider = AdoProvider.CreateProvider(connstr, FactoryType.PostgreSQL))
            {
                var param = new Parameter("select bsm,st_asbinary(geom) as wkb from public.\"DLTB522631\" WHERE objectid=1");
                var tb= provider.Query(param);

                var rt = provider.Query<Item>(param);
                var items = rt.Result;
                List<Geometry> geos = new List<Geometry>();
                foreach (var item in items)
                {
                    WkbParser wkbParser = new WkbParser(item.wkb);
                    var geo = wkbParser.FromReader();
                    geos.Add(geo);
                }
            }
        }

        static void TestVctFormat()
        {
            VctParser vct = new VctParser(@"E:\2001H2019522623.vct", Encoding.GetEncoding("GB2312"));
            var vctInfo= vct.FromReader();
        }

        static void TestFromEsriMdb()
        {
            string connstr = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=F:\test.mdb;";
            using (IAdoProvider provider = AdoProvider.CreateProvider(connstr, FactoryType.OleDb))
            {
                var rt = provider.Query<Item>(new Parameter("select top 1 SHAPE as wkb from testd"));
                var items = rt.Result;
                List<Geometry> geos = new List<Geometry>();
                foreach (var item in items)
                {
                    EsriMdbParser wkbParser = new  EsriMdbParser(item.wkb);
                    var geo = wkbParser.FromReader();

                    //生成wkt
                   // string wkt= geo.ToWkt();

                    geos.Add(geo);
                }
            }
        }

        static void TestArea()
        {
            string wktstr = "POLYGON ((36379440.1493 2936717.206599999, 36379425.4384 2936710.4860999994, 36379423.0042 2936716.307, 36379437.133 2936723.318499999, 36379440.1493 2936717.206599999))";
            //string wktstr = "POLYGON((0 0,0 2,2 2,2 0,0 0))";
            //using (FileStream fs = new FileStream("D:\\wkt.txt", FileMode.Open))
            //{
            //    using (StreamReader reader = new StreamReader(fs))
            //    {
            //        wktstr = reader.ReadToEnd();
            //    }
            //}

            GeometryPlane plane = new GeometryPlane();
            var area = plane.Area(wktstr);

            GeometryEllipse geo = new GeometryEllipse();
            var ellipse = geo.Area(wktstr);

            var dist = geo.Distance(new Coordinate()
            {
                X = 36379440.1493,
                Y = 2936717.206599999
            },
             new Coordinate()
             {
                 X = 36379425.4384,
                 Y = 2936710.4860999994
             });

            var geopoint = new Coordinate()
            {
                X = 36367729.9624,
                Y = 2941185.8308
            };

            var bl = plane.XYtoLB(geopoint);
            var bxy = plane.LBtoXY(bl);
        }

        static void TestGeoJson()
        {
            string file = "C:\\3DCity.json";// AppContext.BaseDirectory + "testfiles\\feature.geojson";//"C:\\3DCity.json";
            string content = File.ReadAllText(file, Encoding.UTF8);
            GeoJsonParser json = new GeoJsonParser(content);

            var geo = json.ToFeatures<attr, JsonMultiPolygon>();

            List<double[]> coords = new List<double[]>();
            coords.Add(new double[] { 1, 2 });
            coords.Add(new double[] { 2, 3 });

            var collection = new FeatureCollection<attr, JsonLineString>()
            {
                name = "八渡镇",
                features = new Feature<attr, JsonLineString>[] {
                 new Feature<attr, JsonLineString>(){
                 properties=new attr(){  name="乃言村",code="522327"},
                 geometry=new JsonLineString(){
                 coordinates=coords
              }
             }
            }
            };
            var str = json.ToWrite<attr, JsonLineString>(collection);
        }
    }

    public class attr
    {
        public string name { get; set; }

        public string code { get; set; }
    }

    public class Item
    {
        public string bsm { get; set; }

        public byte[] wkb { get; set; }

    }

}
